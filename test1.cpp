#include <iostream>
#include <cstdio>
#include <ctime>
#include "NNMatrix.h"
#include "NNException.h" 

using namespace std;

int main() {
	int len = 50;

	NNMatrix * mx1 = randmx( len )->add( NNMatrix::identy( len )->mul( 5 ), 1 );
	NNVector * vc1 = randvc( len, 2 );
	vc1->print();

	//mx1->print();
	//vc1->print();

	time_t start = clock();

	/*
	cout<< "Basic Operations" <<endl;
	double c = dpt( vc1, vc1 );
	cout<< "vc1 dpt :" << c <<endl;
	NNVector* vc2 = mmv( mx1, vc1 );
	vc2->print();
	NNMatrix* mx2 = mmx( mx1, mx1 );
	mx2->print();
	delete vc2, delete mx2;
*/

	//rc1->print();
	//NNVector* rc2 = mmv( mx1, rc1 );
	//rc2->print();


	cout<< "Test Cholesky Decompostion Method" <<endl;
	NNVector* rc1 = cholSolve( mx1, vc1 );
	NNVector* vc2 = mmv( mx1, rc1 );
	vc2->print();
	//rc1->print();
	//rc2 = mmv( mx1, rc1);
	//rc2->print();
	delete rc1, delete vc2;// delete rc2;

	/*
	cout << "Test LU Decomp results" <<endl;
	NNMatrix * L = new NNMatrix( len, len );
	NNMatrix * U = new NNMatrix( len, len );
	int * pt = new int[len];
	mx1->swap( 0, 3 );
	mx1->swap( 1, 5 );
	mx1->swap( 1, 7 );
	mx1->swap( 2, 5 );
	luDecomp( mx1, L, U, pt );
	mx2 = mmx( L, U );
	mx2->print();
	mx1->print();
	delete mx2, delete L, delete U, delete pt;

	cout<< "Testing LU Decomposition Method" <<endl;
	rc1 = luSolve( mx1, vc1 );
	rc1->print();
	rc2 = mmv( mx1, rc1 );
	rc2->print();
	vc1->print();
	delete rc1, delete rc2;

	//mx1->swap( 3, 0 );
	cout<< "Test Gauss Jordan Elimination Method" <<endl;
	NNMatrix* mx1r = mx1->clone();
	gaussElim( mx1r );
	mx2 = mmx( mx1r, mx1 );
	cout<< "Print mx2, identiy matrix" <<endl;
	mx2->print();
	rc1 = mmv( mx1r, vc1 );
	rc1->print();
	rc2 = mmv( mx1, rc1 );
	cout<< "recover vc1" <<endl;
	rc2->print();
	cout<< "vc1 is: " <<endl;
	vc1->print();
	delete mx1r, delete mx2, delete rc1, delete rc2;

	delete mx1, delete vc1;
	*/


}
