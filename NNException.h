#ifndef _NNEXCEPTION_
#define _NNEXCEPTION_
#include <exception>
#include <string>

class NNVector;
class NNMatrix;

class DimException : public std::exception {
	private:
		std::string str;
	public:
		DimException( NNVector * nnv1, NNVector * nnv2 );
		DimException( NNMatrix * nnm1, NNMatrix * nnm2 );
		DimException( NNVector * nnv,  NNMatrix * nnm );
		DimException( NNMatrix * nnm,  NNVector * nnv );
		const char* what() const throw() { return str.c_str(); };
		~DimException() throw(){};
};

class MatrixException: public std::exception {
	private:
		std::string str;
	public:
		MatrixException( std::string _str ) { str = _str; };
		const char* what() const throw() { return str.c_str(); };
		~MatrixException() throw(){};
};
#endif
