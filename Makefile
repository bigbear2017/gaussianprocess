default: test1 test2 
HOME = ..
CC = g++

SRCS := \
	NNMatrix.cpp \
	NNException.cpp \
	NNFunMin.cpp \
	NNCov.cpp \
	NNGp.cpp \
	NNLinReg.cpp \

OBJS := ${SRCS:.cpp=.o}
CFLAGS = -O2

test1 : test1.o  $(OBJS)
	$(CC) $(CFLAGS) -o test1 test1.o $(OBJS)

test2 : test2.o  $(OBJS)
	$(CC) $(CFLAGS) -o test2 test2.o $(OBJS)

%.o: %.cpp *.h
	$(CC) $(CFLAGS)  -c $< -o $@

clean:
	rm *.o
	rm test1
	rm test2
