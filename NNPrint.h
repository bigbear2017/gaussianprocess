/*
 * Since I have only defined the double matrix, I want to print the double matrix beautifully.
 * The same as the array.
 */
#ifndef _NNPRINT_
#define _NNPRINT_
#include <cstdio>
#include <cfloat>
#include <cstring>

#include "NNArray.h"
class NNPrint { 
	public:
		static void printDoubleArray( double * da, int size, int precision);
		static void printDoubleMatrix( double ** dm, int nr, int nc, int precision );
};

void NNPrint::printDoubleArray( double * da, int size, int pre ) {
	int maxdex = NNArray::maxdex( da, size );
	double max = da[maxdex];

	int v = (int) max / 10;
	int width = 1;
	while( v > 0 ) {
		width ++ ;
		v = v / 10;
	}

	int mindex = NNArray::mindex( da, size );
	double min = da[mindex];
	if( min < 0 ) {
		v = (int) (-min) / 10;
		int width2 = 2;
		while( v > 0 ) {
			width2 ++ ;
			v = v / 10;
		}
		if ( width2 > width ) {
			width = width2;
		}
	}

	int lineNum = 13;
	lineNum = lineNum - width + 1;
	width = width + pre + 1;
	char str[20];
	snprintf( str, 20, "%%%d.%df  ", width, pre );
	for( int i = 0; i < size; i++ ) {
		if( i > 0 && i % lineNum == 0 ) {
			printf( "\n" );
		}
		printf( str, da[i] );
	}
	printf( "\n" );
}

void NNPrint::printDoubleMatrix( double ** dm, int nr, int nc, int pre ) {
	double max = DBL_MIN;
	for( int i = 0; i < nr; i++ ) {
		for( int j = 0; j < nc; j++ ) {
			if( dm[i][j] > max ) {
				max = dm[i][j];
			}
		}
	}
	int v = (int) max / 10;

	int width = 1;
	while( v > 0 ) {
		width ++ ;
		v = v / 10;
	}

	double min = DBL_MAX;
	for( int i = 0; i < nr; i++ ) {
		for( int j = 0; j < nc; j++ ) {
			if( dm[i][j] < min ) {
				min = dm[i][j];
			}
		}
	}
	if( min < 0 ) {
		v = (int) (-min) / 10;
		int width2 = 2;
		while( v > 0 ) {
			width2 ++ ;
			v = v / 10;
		}
		if ( width2 > width ) {
			width = width2;
		}
	}


	int lineNum = 13;
	lineNum = lineNum - width + 1;
	width = width + pre + 1;
	char str[20];
	snprintf( str, 20, "%%%d.%df  ", width, pre );
	for( int i = 0; i < nr; i++ ) {
		for( int j = 0; j < nc; j++ ) {
			if( j >0 && j % lineNum == 0 ) {
				printf( "\n" );
			}
			printf( str, dm[i][j] );
		}
		printf( "\n" );
	}
	printf( "\n" );
}
#endif
