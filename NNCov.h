#ifndef _NNCOV_
#define _NNCOV_
#include "NNMatrix.h" 
/*
 * The file will contain all kinds of covariance functions
 * I hope that I can design a class that no matter which 
 * covariance function I use, I don't need to write a new
 * train function or prediction function.
 * Now I am not sure how to design this class. 
 * So, I start to write it. We will know it later.
 */
/*
 * NNCov is base virtual class, all covariance should implement
 * the two functions in this class.
 */

class NNCov{
	public:
	/* return the covarian matrix
	 * Parameter:
	 * loghyp: include all hype parameters, signal variance, 
	 * length scales or whatever
	 * d1: the first data matrix, each row should be a data vector
	 * d2: the second data matrix, each row should be a data vector
	 * Output:
	 * if d2 != null, it will return kxy
	 * if d2 == null, it will return kxx
	 */
	virtual NNMatrix* cf( NNVector* loghyp, NNMatrix* d1, NNMatrix* d2) = 0;
	/* This function will return the derivative of signal variance
	 * in a matrix form
	 * Parameter:
	 * hyp: the hype paramters in each stage,
	 * d: the data matrix
	 * i: the ith hype paramter
	 */
	virtual NNMatrix* dvt( NNVector* loghyp, NNMatrix* d, int i ) = 0;
};

class NNCovSe : public NNCov{
	public:
		/*
		 * Paramters:
		 * hyp: should be like, signal variance, 
		 * length scale1, length scale2, ... (without noise variance)
		 */
		NNMatrix* cf( NNVector* loghyp, NNMatrix* d1, NNMatrix* d2 );
		/*
		 * Paramters:
		 * hyp: should be like, signal variance,
		 * length scale1, length scale2, ... (without noise variance)
		 */
		NNMatrix* dvt( NNVector* loghyp, NNMatrix* d, int index );
};

















#endif
