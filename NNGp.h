#ifndef _NNGP_
#define _NNGP_
#include "NNMatrix.h"
#include "NNCov.h" 
#include "NNFunMin.h"
 
class NNGp {
	private:

	public:
		/* This function will return the posterior mean vector of Gaussian Process regression
		 * Parameters:
		 * hyp: the hype paramters which include signal variance, and other length scales
		 * lnv : the noise variance 
		 * cf : the covariance function which should implement the two functions
		 * ix : the input data matrix, each row should be a data vector
		 * iy : the input data labels.
		 * tx : the test data matrix, each row should be a data vector
		 * prior : the prior mean value. If have no prior, set it 0. 
		 */
		static NNVector* regm( NNVector* loghyp, double lnv, NNCov* cf, NNMatrix* ix, NNVector* iy, NNMatrix* tx, double prior );
		/* This function will return the posterior variance matrix of Gaussian Process regression
		 * Paramaters:
		 * loghyp: the hype paramters which include signal variance, and other length scales
		 * lnv : the noise variance 
		 * cf : the covariance function which should implement the two functions
		 * ix : the input data matrix, each row should be a data vector
		 * iy : the input data labels.
		 */
		static NNMatrix* regv( NNVector* loghyp, double lnv, NNCov* cf, NNMatrix* ix, NNVector* iy, NNMatrix* tx);
		/* This function is used to train the gaussian process.
		 * Paramaters:
		 * loghyp: the hype paramters which include signal variance, and other length scales
		 * lnv : the noise variance 
		 * cf : the covariance function which should implement the two functions
		 * ix : the input data matrix, each row should be a data vector
		 * iy : the input data labels.
		 */
		static void train( NNVector* loghyp, double &lnv, double &prior, NNCov* cf, NNMatrix* ix, NNVector* iy );
		/* This function is used to calculate the log mariginal likelihood 
		 * K: the covariance matrix
		 * y: the input labels
		 */
		static double lmlh ( NNMatrix* K, NNVector* y );
};

class NNGpMin : public NNFunMin {
	private:
		NNMatrix* ix;
		NNVector * iy;
		NNCov* cf;
		int ni;

	public:
		NNGpMin( NNMatrix * ix_, NNVector* iy_, NNCov* cf_ );
		virtual double func( NNVector* loghypv );
		virtual NNVector* fund( NNVector* loghypv );
};
#endif
