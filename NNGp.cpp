#include <cstddef>
#include <cmath>
#include <iostream>
#include "NNGp.h"
#include "NNCov.h"
#include "NNMatrix.h"

using namespace std;

NNVector* NNGp::regm( NNVector* loghyp, double lnv, NNCov* cf, NNMatrix* ix, NNVector* iy, NNMatrix* tx, double prior) {
	double nv = exp(lnv);
	NNVector* ty = NULL;
	int ni = ix->nrows(); // the number of input values
	NNVector* iyc = iy->clone(); // to delete
	NNVector* piy = new NNVector( ni, prior );  // to delete
	iyc -> sub( piy ); // substract ( y - z_y )

	int nt = tx->nrows(); // the number of test values
	NNVector* pty = new NNVector( nt, prior ); // to delete
	NNMatrix* nm = NNMatrix::identy( ni ) ->mul( nv * nv );
	NNMatrix* K = cf->cf( loghyp, ix, ix )->add( nm );
	NNMatrix* Ks = cf->cf( loghyp, ix, tx );
	ty = Ks->mmv( cholSolve( K, iyc ), 1 )->add( pty );

	delete nm, delete piy, delete pty, delete iyc;
	delete K, delete Ks;

	return ty;
}

NNMatrix* NNGp::regv( NNVector* loghyp, double lnv, NNCov* cf, NNMatrix* ix, NNVector* iy, NNMatrix* tx) {
	double nv = exp( lnv );
	NNMatrix* pv = NULL;
	int nt = tx->nrows();
	int ni = ix->nrows();

	NNMatrix* nm = NNMatrix::identy( ni )->mul( nv * nv );
	NNMatrix* K =  cf->cf( loghyp, ix, ix )->add( nm );
	NNMatrix* Ks = cf->cf( loghyp, ix, tx );
	NNMatrix* Kt = cf->cf( loghyp, tx, tx );

	pv = Kt->sub( Ks->mmx( mCholSolve( K, Ks ), 1 ) );
	delete nm, delete Ks, delete K;
	return pv;
}

void NNGp::train( NNVector* loghyp, double &lnv, double &prior, NNCov* cf, NNMatrix* ix, NNVector* iy) {
	double cc = 1;
	int ni = ix->nrows();
	int len = loghyp->len();

	double p = 0;
	for( int i = 0; i < ni; i++ ) {
		p += (*iy)[i];
	}
	prior = p / ni;

	NNVector* loghypv = new NNVector( len + 1 );
	for( int i = 0; i < len; i++ ) {
		(*loghypv)[i] = (*loghyp)[i];
	}
	(*loghypv)[len] = lnv;
	NNGpMin gm ( ix, iy, cf );
	gm.min( loghypv );

	for( int i = 0; i < len; i++ ) {
		(*loghyp)[i] = (*loghypv)[i];
	}
	lnv = (*loghypv)[len];
}

double NNGp::lmlh( NNMatrix* K, NNVector* y ) {
	NNMatrix* L = cholesky( K );
	NNVector* alp = linearSolve( L, y );
	double v1 = y->dpt( alp, 1 );
	double v2 = 0;
	for( int i = 0; i < L->nrows(); i++ ) {
		v2 += log( (*L)[i][i] );
	}
	v2 = v2 * 2;
	double ni = y->len();
	double r = - v1 * 0.5 - v2 * 0.5 - ni * 0.5 * log( 2 * PI );
	return r;
}


NNGpMin::NNGpMin( NNMatrix * ix_, NNVector* iy_, NNCov* cf_ ){
	ix = ix_;
	iy = iy_;
	cf = cf_;
	ni = ix->nrows();
}

double NNGpMin::func( NNVector* loghypv ) {
	int len = loghypv->len() - 1;
	NNVector* loghyp = new NNVector( len );
	for( int i = 0; i < len; i++ ) {
		(*loghyp)[i] = (*loghypv)[i];
	}
	double nv = exp((*loghypv)[ len ]);
	NNMatrix* K = cf->cf( loghyp, ix, ix );
	NNMatrix* nm = NNMatrix::identy( ni )->mul( nv * nv );
	K->add( nm, 1 );
	double r = NNGp::lmlh( K, iy );
	delete K, delete loghyp;
	return -r;
}

NNVector* NNGpMin::fund( NNVector* loghypv ) {
	int len = loghypv->len() - 1;
	NNVector* loghyp = new NNVector( len );
	for( int i = 0; i < len; i++ ) {
		(*loghyp)[i] = (*loghypv)[i];
	}
	double nv = exp((*loghypv)[ len ]);
	NNMatrix* noise = NNMatrix::identy( ni )->mul( nv * nv );
	NNMatrix* K = cf->cf( loghyp, ix, ix )->add( noise );

	NNVector* alp = cholSolve( K, iy );
	NNMatrix* aa  = vvm ( alp, alp );

	NNMatrix * Kc = K->clone();
	gaussElim( Kc );

	aa->sub( Kc );

	NNVector* hgt = new NNVector( len + 1 );
	for( int i = 0; i < len; i++ ) {
		NNMatrix* Kt = cf->dvt( loghyp, ix, i );
		NNMatrix* r = mmx( aa, Kt );
		double gdt1 = trace( r );
		//cout<< "gdt : " << gdt;
		(*hgt)[i] = -gdt1 / 2;
		delete Kt, delete r;
	}
	//NNMatrix * ndvt = NNMatrix::identy( nc )->mul( nv * nv * 2 );
	//NNMatrix* nov = mmx( aa, ndvt );
	double gdt2 = 2*nv*nv * trace(aa);
	//cout<< "gdt : " << gdt;
	(*hgt)[len] = -gdt2 / 2;

	delete loghyp, delete noise, delete K, delete alp;
	delete aa, delete Kc; // delete ndvt, delete nov;
	return hgt;
}
