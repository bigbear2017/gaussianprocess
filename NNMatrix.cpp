#include "NNMatrix.h"
#include "NNException.h" 

#include <cstdio>
#include <iostream>
#include <cfloat>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cassert>
#include <exception>
#include <stdexcept>

#include "NNPrint.h"
using namespace std;


//************************************
//************************************
//*******Definition of NNVector*******
//************************************
//************************************
NNVector::NNVector( int _size ) {
	size = _size;
	ta = new double [size];
	memset( ta, 0, sizeof( double ) * size );
}

NNVector::NNVector( int _size, double v ) {
	size = _size;
	ta = new double[size];
	std::fill( ta, ta + size, v );
}

NNVector* NNVector::add( NNVector* nnv, int del ) {
	for( int i = 0; i < size; i++ ) {
		ta[i] += nnv->ta[i];
	}

	if( del == 1 ) {
		delete nnv;
	}
	return this;
}


NNVector* NNVector::sub( NNVector* nnv, int del ) {
	for( int i = 0; i < size; i++ ) {
		ta[i] -= nnv->ta[i];
	}

	if ( del == 1 ) {
		delete nnv;
	}

	return this;
}



NNVector* NNVector::mul( double v ) {
	for( int i = 0; i < size; i++ ) {
		ta[i] *= v;
	}

	return this;
}



NNVector* NNVector::div( double v ) {
	for( int i = 0; i < size; i++ ) {
		ta[i] /= v;
	}
	return this;
}

NNVector& NNVector::operator+( NNVector& nnv ) {
	for( int i = 0; i < size; i++ ) {
		ta[i] += nnv.ta[i];
	}
	return *this;
}

NNVector& NNVector::operator-( NNVector& nnv ) {
	for( int i = 0; i < size; i++ ) {
		ta[i] -= nnv.ta[i];
	}
	return *this;
}

NNVector& NNVector::operator*( double v ) {
	for( int i = 0; i < size; i++ ) {
		ta[i] *= v;
	}
	return *this;
}


NNVector& NNVector::operator/( double v ) {
	for( int i = 0; i < size; i++ ) {
		ta[i] /= v;
	}
	return *this;
}

double NNVector::dpt( NNVector* nnv, int del ) {
	if( size != nnv->size ) {
		throw invalid_argument("No dot product, len don't match");
	}
	double sum = 0;
	for( int i = 0; i < size; i++ ) {
		sum += (ta[i] * nnv->ta[i]);
	}
	if( del == 1 ) {
		delete nnv;
	}
	return sum;
}

double& NNVector::operator[]( int index ){
	return ta[index];
}

NNVector* NNVector::clone() const {
	NNVector * nnv = new NNVector( size );
	for( int i = 0; i < size; i++ ) {
		nnv->ta[i] = ta[i];
	}
	return nnv;
}

void NNVector::print( int pre ) {
	NNPrint::printDoubleArray( ta, size, pre );
}

NNVector* NNVector::mmv( NNMatrix* nnm, int del ) {
	if ( size != nnm->nr ){
		throw DimException( this, nnm );
	}
	NNVector* r = new NNVector( nnm->nc );
	for( int i = 0; i < nnm->nc; i++ ) {
		double sum = 0;
		for( int j = 0; j < nnm->nr; j++ ) {
			sum += ta[j] * nnm->tm[j][i];
		}
		r->ta[i] = sum;
	}
	if( del == 1 ) {
		delete nnm;
	}
	return r;
}

//*************************************************
//*************************************************
//******Definition of NNMatrix*********************
//*************************************************
//*************************************************
NNMatrix::NNMatrix( int _nr, int _nc ) {
	nr = _nr;
	nc = _nc;
	tm = new double* [nr];
	for( int i = 0; i < nr; i++ ) {
		tm[i] = new double [nc];
		memset( tm[i], 0, sizeof(double) * nc );
	}
}

NNMatrix::NNMatrix( int _nr, int _nc, double v) {
	nr = _nr;
	nc = _nc;
	tm = new double* [nr];
	for( int i = 0; i < nr; i++ ) {
		tm[i] = new double [nc];
		std::fill( tm[i], tm[i] + nc, v );
	}
}

NNMatrix* NNMatrix::swap( int i, int j ) {
	double * temp = tm[i];
	tm[i] = tm[j];
	tm[j] = temp;
	return this;
}

NNMatrix* NNMatrix::identy( int _nr) {
	NNMatrix* r = new NNMatrix( _nr, _nr );
	for( int i = 0; i < _nr; i++ ) {
		r->tm[i][i] = 1;
	}
	return r;
}


NNMatrix* NNMatrix::add( NNMatrix* nnm, int del ) {
	for( int i = 0; i < nr; i++ ) {
		for( int j = 0; j < nc; j++ ) {
			tm[i][j] += nnm->tm[i][j];
		}
	}
	if( del == 1 ) {
		delete nnm;
	}
	return this;
}

NNMatrix* NNMatrix::sub( NNMatrix* nnm, int del ) {
	for( int i = 0; i < nr; i++ ) {
		for( int j = 0; j < nc; j++ ) {
			tm[i][j] -= nnm->tm[i][j];
		}
	}
	if( del == 1 ) {
		delete nnm;
	}
	return this;
}

NNMatrix* NNMatrix::mul( double v ) {
	for( int i = 0; i < nr; i++ ) {
		for( int j = 0; j < nc; j++ ) {
			tm[i][j] *= v;
		}
	}
	return this;
}

NNMatrix* NNMatrix::div( double v ) {
	for( int i = 0; i < nr; i++ ) {
		for( int j = 0; j < nc; j++ ) {
			tm[i][j] /= v;
		}
	}
	return this;
}

NNMatrix& NNMatrix::operator+ ( NNMatrix& nnm ) {
	for( int i = 0; i < nr; i++ ) {
		for( int j = 0; j < nc; j++ ) {
			tm[i][j] += nnm.tm[i][j];
		}
	}
	return *this;
}
NNMatrix& NNMatrix::operator- ( NNMatrix&  nnm ) {
	for( int i = 0; i < nr; i++ ) {
		for( int j = 0; j < nc; j++ ) {
			tm[i][j] -= nnm.tm[i][j];
		}
	}
	return *this;
}
NNMatrix& NNMatrix::operator* ( double v ) {
	for( int i = 0; i < nr; i++ ) {
		for( int j = 0; j < nc; j++ ) {
			tm[i][j] *= v;
		}
	}
	return *this;
}

NNMatrix& NNMatrix::operator/ ( double v ) {
	for( int i = 0; i < nr; i++ ) {
		for( int j = 0; j < nc; j++ ) {
			tm[i][j] /= v;
		}
	}
	return *this;
}


NNMatrix* NNMatrix::mmx( NNMatrix* nnm, int del ) {
	if( nc != nnm->nr ) {
		throw DimException( this, nnm );
	}

	//double ** r = new double[nr][nnm->nc];
	NNMatrix* r = new NNMatrix( nr, nnm->nc );
	for( int i = 0; i < nr; i++ ) {
		for( int j = 0; j < nnm->nc; j++ ) {
			double sum = 0;
			for ( int k = 0; k < nc; k++ ) {
				sum += tm[i][k] * nnm->tm[k][j];
			}
			r->tm[i][j] = sum;
		}
	}
	if( del == 1 ) {
		delete nnm;
	}
	return r;
}


NNVector* NNMatrix::mmv( NNVector* nnv, int del ) {
	if( nc != nnv->size ){
		throw DimException( this, nnv );
	}
	NNVector* r = new NNVector( nr );
	for( int i = 0; i < nr; i++ ) {
		double sum = 0;
		for( int j = 0; j < nc; j++ ) {
			sum += (tm[i][j] * nnv->ta[j]);
		}
		r->ta[i] = sum;
	}
	if( del == 1 ) {
		delete nnv;
	}
	return r;
} 

NNMatrix* NNMatrix::transpose() { 
	return tp();
}
NNMatrix* NNMatrix::tp() {
	double ** old = tm;
	tm = new double* [nc] ;
	for( int i = 0; i < nc; i++ ) {
		tm[i] = new double[nr];
	}

	int temp = nr;
	nr = nc;
	nc = temp;

	for( int i = 0; i < nr; i++ ) {
		for( int j = 0; j < nc; j++ ) {
			tm[i][j] = old[j][i];
		}
	}

	//remember that nc is the number of rows of old
	for( int i = 0; i < nc; i++ ) {
		delete [] old[i];
	}
	delete [] old;

	return this;
}

NNMatrix* NNMatrix::clone() const {
	NNMatrix * r = new NNMatrix( nr, nc );
	for( int i = 0; i < nr; i++ ) {
		memcpy( r->tm[i], tm[i], sizeof( double ) * nc );
	}
	return r;
}

void NNMatrix::print( int pre ) {
	NNPrint::printDoubleMatrix( tm, nr, nc, pre );
}
//*******************************************
//*******************************************
//*******************************************
//*********** ALL OPERATIONS ****************
//*******************************************
//*******************************************
//*******************************************
double dpt( NNVector* nnv1, NNVector* nnv2 ) {
	if( nnv1->size != nnv2->size ) {
		throw DimException( nnv1, nnv2 );
	}
	double sum = 0;
	for( int i = 0; i < nnv1->size; i++ ) {
		sum += (nnv1->ta[i] * nnv2->ta[i]);
	}
	return sum;
}

NNMatrix * vvm( NNVector* nnv1, NNVector* nnv2 ) {
	int nr = nnv1->len();
	int nc = nnv2->len();
	NNMatrix* r = new NNMatrix ( nr, nc );
	for( int i = 0; i < nr; i++ ) {
		for( int j = 0; j < nc; j++ ) {
			(*r)[i][j] = (*nnv1)[i] * (*nnv2)[j];
		}
	}
	return r;
}

NNVector* mmv( NNVector* nnv, NNMatrix* nnm ) {
	if ( nnv->size != nnm->nr ){
		throw DimException( nnv, nnm );
	}
	NNVector* r = new NNVector( nnm->nc );

	for( int i = 0; i < nnm->nc; i++ ) {
		double sum = 0;
		for( int j = 0; j < nnm->nr; j++ ) {
			sum += ( nnv->ta[j] * nnm->tm[j][i] );
		}
		r->ta[i] = sum;
	}
	return r;
}

NNVector* mmv( NNMatrix* nnm, NNVector* nnv ) {
	if( nnm->nc != nnv->size ){
		throw DimException( nnm, nnv );
	}

	NNVector* r = new NNVector( nnm->nr );
	for( int i = 0; i < nnm->nr; i++ ) {
		double sum = 0;
		for( int j = 0; j < nnm->nc; j++ ) {
			sum += (nnm->tm[i][j] * nnv->ta[j]);
		}
		r->ta[i] = sum;
	}
	return r;
}

NNMatrix* mmx( NNMatrix* nnm1, NNMatrix* nnm2) {
	if( nnm1->nc != nnm2->nr ) {
		throw DimException( nnm1, nnm2 );
	}

	//double ** r = new double[nr][nnm->nc];
	NNMatrix* r = new NNMatrix( nnm1->nr, nnm2->nc );
	for( int i = 0; i < nnm1->nr; i++ ) {
		for( int j = 0; j < nnm2->nc; j++ ) {
			double sum = 0;
			for ( int k = 0; k < nnm1->nc; k++ ) {
				sum += nnm1->tm[i][k] * nnm2->tm[k][j];
			}
			r->tm[i][j] = sum;
		}
	}
	return r;
}

double vmv( NNVector* v1, NNMatrix* m, NNVector* v2 ) { 
	return v1->dpt( mmv( m, v2 ), 1 );
}


NNVector* randvc(int len) {
	srand( time(NULL) );
	
	NNVector* nnv = new NNVector( len );
	for( int i = 0; i < len; i++ ) {
		double r = rand() /(double) RAND_MAX;
		(*nnv) [i] = r;
	}
	return nnv;
}

NNVector* randvc( int len, double v ) {
	srand( time(NULL) );

	NNVector* nnv = new NNVector( len );
	for( int i = 0; i < len; i++ ) {
		double r = rand() / (double) RAND_MAX;
		(*nnv) [i] = r * v;
	}
	return nnv;
}

NNMatrix* randmx(int nr) {
	srand( time(NULL) );

	NNMatrix* nnm = new NNMatrix( nr, nr );
	for( int i = 0; i < nr; i++ ) {
		for( int j = 0; j < nr; j++ ) {
			double r = rand() / (double) RAND_MAX;
			(*nnm) [i][j] = r;
			(*nnm) [j][i] = r;
		}
	}

	return nnm;
}

NNMatrix* randmx( int nr, double v, double d ) {
	srand( time(NULL) );

	NNMatrix* nnm = new NNMatrix( nr, nr );
	for( int i = 0; i < nr; i++ ) {
		for( int j = 0; j < nr; j++ ) {
			double r = rand() / (double) RAND_MAX;
			(*nnm) [i][j] = r * v;
			(*nnm) [j][i] = r * v;
		}
	}

	for( int i = 0; i < nr; i++ ) {
		double r = rand() / (double) RAND_MAX;
		(*nnm) [i][i] = r * d;
	}

	return nnm;
}

NNMatrix* tp( NNMatrix * nnm ) {
	int nr = nnm->nrows();
	int nc = nnm->ncols();
	NNMatrix* r = new NNMatrix( nc, nr );
	for( int i = 0; i < nc; i++ ) {
		for( int j = 0; j < nr; j++ ) {
			r->tm[i][j] = nnm->tm[j][i]; 
		}
	}
	return r;
}

double trace( NNMatrix * m ) {
	int nr = m->nrows();
	double sum = 0;
	for( int i = 0; i < nr; i++ ) {
		sum += (*m)[i][i];
	}
	return sum;
}

double det( NNMatrix * m ) {
	int nr = m->nrows();
	int nc = m->ncols();
	NNMatrix* L = new NNMatrix( nr, nc );
	NNMatrix* U = new NNMatrix( nr, nc );
	int * pt = new int[ nr ];
	try {
		luDecomp( m, L, U, pt );
	} catch ( exception & e ) {
		cout<< e.what();
		return 0;
	}
	double dt = 1;
	for( int i = 0; i < nr; i++ ) {
		dt *= (*U)[i][i];
	}
	delete L, delete U, delete pt;
	return dt;
}

double ldet( NNMatrix * m ) {
	int nr = m->nrows();
	int nc = m->ncols();
	NNMatrix* L = new NNMatrix( nr, nc );
	NNMatrix* U = new NNMatrix( nr, nc );
	int * pt = new int[ nr ];
	luDecomp( m, L, U, pt ); // don't catch error
	double dt = 0;
	for( int i = 0; i < nr; i++ ) {
		dt += log((*U)[i][i]);
	}
	delete L, delete U, delete pt;
	return dt;
}

double cholDet( NNMatrix * m ) {
	NNMatrix * L = cholesky( m );
	int nr = m-> nrows();
	double sum = 1;
	for( int i = 0; i < nr; i++ ) {
		sum *= (*L)[i][i] * (*L)[i][i];
	}
	delete L;
	return sum;
}
double lcholDet( NNMatrix * m ) {
	NNMatrix * L = cholesky( m );
	int nr = m-> nrows();
	double sum = 0;
	for( int i = 0; i < nr; i++ ) {
		sum += 2*log((*L)[i][i]);
	}
	delete L;
	return sum;
}

double norm2( NNVector* v ) {
	double r = dpt( v, v );
	return sqrt( r );
}

NNVector* conjugate( NNMatrix * A, NNVector* b ) {
	int len = b->len();
	int len10 = len * 10;

	NNVector* x = new NNVector( len, 0 ); // 1 objs
	NNVector* r = b->clone()->sub( mmv( A, x ), 1 ); // 2 objs


	double rr = dpt( r, r );
	int i = 1;

	NNVector* p = new NNVector( len, 0 ); // 3 objs
	NNVector* rm1 = r->clone(); // 4 objs
	NNVector* rm2 = r->clone(); // 5 objs
	NNVector* pm1 = p->clone(); // 6 objs
	while( i < len10 && rr > 0.000001 ) {
		if ( i == 1 ) {
			p->add( r );
		} else {
			double beta = dpt( rm1, rm1 ) / dpt( rm2, rm2 );
			p = rm1->clone()->add( pm1->mul( beta ) ); // 7 objs
		}
		NNVector* ap = mmv( A, p ); // 8 objs
		double alp = dpt( rm1, rm1 ) / dpt( p, ap );

		x->add( p->clone()->mul( alp ), 1 ); // 8 objs

		r->sub( ap->mul( alp ), 1 ); // 7 objs

		rr = dpt( r, r );
		delete rm2; //6 objs
		rm2 = rm1;
		rm1 = r->clone(); // 6 objs

		delete pm1; // 5 objs
		pm1 = p;
		i++;
	}
	delete p, delete rm2, delete rm1, delete r; // 1 objs
#ifdef DEBUG
	cout<< "i is:" << i << " rr is:" << rr <<endl;
#endif

	return x; // returned
}

NNMatrix* cholesky ( NNMatrix * A ) {
	int nrow = A->nrows();
	NNMatrix* r = new NNMatrix( nrow, nrow );
	for( int i = 0; i < nrow; i++ ) {
		for( int j = 0; j <= i; j++ ) {
			if( i == j ) {
				int s = j - 1;
				double sqk = 0;
				for( int k = 0; k <= s; k++ ) {
					sqk += ( (*r)[i][k] * (*r)[i][k]);
				}
				(*r)[i][j] =  sqrt( (*A)[j][j] - sqk );
			} else {
				int s = j - 1;
				double sqk = 0;
				for( int k = 0; k <=s; k++ ) {
					sqk += (*r)[i][k] * (*r)[j][k];
				}
				(*r)[i][j] = ((*A)[i][j] - sqk) / (*r)[j][j];
			}
		}
	}
	return r;
}
NNVector* fsub( NNMatrix * L, NNVector* b ) {
	if ( L->nrows() != b->len() ) {
		throw DimException( L, b );
	}
	int len = b->len();
	NNVector* r = new NNVector( len );
	for( int i = 0; i < len; i++ ) {
		double sum = 0;
		for( int j = 0; j < i; j++ ) {
			sum += ( (*L)[i][j] * (*r)[j] );
		}
		(*r)[i] = ((*b)[i] - sum ) / (*L)[i][i];
	}
	return r;
}
NNVector* bsub( NNMatrix * Lt, NNVector* b ) {
	if ( Lt->nrows() != b->len() ) {
		throw DimException( Lt, b );
	}
	int len = b->len();
	NNVector* r = new NNVector( len );
	for( int i = len - 1; i >= 0; i-- ) {
		double sum = 0;
		for( int j = len - 1; j > i; j-- ) {
			sum += ( (*Lt)[i][j] * (*r)[j] );
		}
		(*r)[i] = ((*b)[i] - sum ) / (*Lt)[i][i];
	}
	return r;
}


NNVector* linearSolve( NNMatrix* L, NNVector* b ) {
	NNMatrix* LT= tp( L );

	NNVector* y = fsub( L , b );
	NNVector* r = bsub( LT, y );

	delete LT, delete y;

	return r;
}


NNVector* cholSolve( NNMatrix * A, NNVector* b ){
	NNMatrix* L = cholesky( A );
	NNVector* r = linearSolve( L, b );
	delete L;
	return r;
}

/* Each column of B is a data vector */
NNMatrix* mCholSolve( NNMatrix* A, NNMatrix* B ) {
	int br = B->nrows();
	int bc = B->ncols();

	NNMatrix* L = cholesky( A );
	NNVector* b = new NNVector( br );
	NNMatrix* r = new NNMatrix ( br, bc );

	for( int i = 0; i < bc; i++ ) {
		for( int j = 0; j < br; j++ ) {
			(*b)[j] = (*B)[j][i];
		}

		NNVector* x = linearSolve( L, b );

		for( int j = 0; j < br; j++ ) {
			(*r)[j][i] = (*x)[j];
		}
		delete x;
	}
	delete b, delete L;
	return r;
}

/* This function will do Gauss-Jordan Elimination
 * it will return the inverse of a matrix
 */
void gaussElim( NNMatrix * A, NNMatrix* b) {
	assert( A );
	int nr = A->nrows();
	int nc = A->ncols();
	if( nr != nc ) {
		throw DimException( A, A );
	}
	//NNVector * pivot = new NNVector( nr );
	NNMatrix* y = NNMatrix::identy( nr );
	int pt = 0;


	int bc = 0;
	if( b != NULL ) {
		b->ncols();
	}
	for( int i = 0; i < nr; i ++ ) {

		double max = DBL_MIN;
		//find the largest pivot
		for( int u = i; u < nr; u++ ) {
			if( abs((*A)[u][i]) > max ) {
				pt = u;
				max = abs((*A)[u][i]);
			}
		}

		if( i != pt ) {
			A->swap( pt, i );
			y->swap( pt, i );
			if( b != NULL ) {
				b->swap( pt, i );
			}
		}

		double pv = 1.0 / (*A)[i][i];
		if( pv == 0 || abs(pv) < 0.00001 ) {
			throw runtime_error( "Matrix cannot be inversed!" );
		}

		for( int u = i; u < nc; u++ ) {
			(*A)[i][u] *= pv;
		}
		for( int u = 0; u < nc; u++ ) {
			(*y)[i][u] *= pv;
		}
		for( int u = 0; u < bc; i++ ) { //if b is null, that's fine.
			(*b)[i][u] = (*b)[i][u] / pv;
		}

		for( int u = 0; u < nr; u++ ) {
			if( u != i ) {
				pv = (*A)[u][i];
				for( int j = i; j < nc; j++ ) {
					(*A)[u][j] = (*A)[u][j] - (*A)[i][j] * pv;
				}
				for( int j = 0; j < nc; j++ ) {
					(*y)[u][j] = (*y)[u][j] - (*y)[i][j] * pv;
				}
				for( int j = 0; j < bc; j++ ) {
					(*b)[u][j] = (*b)[u][j] - (*b)[i][j] * pv;
				}
			}
		}
	}

	for( int i = 0; i < nr; i++ ) {
		memcpy( (*A)[i], (*y)[i], sizeof(double) * nc );
	}

	delete y;
}

void gaussElim( NNMatrix* A ) {
	gaussElim( A, NULL );
}

void luDecomp( NNMatrix * A, NNMatrix* L, NNMatrix * U, int * pt ) {
	/* L is a lower triangular matrix 
	 * U is a upper triangular matrix
	 * I will use partial Pivoting
	 */
	assert( A );
	assert( L );
	assert( U );
	assert( pt );

	int nr = A->nrows();
	int nc = A->ncols();

	if( nr != nc || L->nrows() != nr || \
			L->ncols() != nr || \
			U->nrows() != nr || \
			U->ncols() != nr ) {
		throw invalid_argument( "input dimension are not consistent !" );
	}

	for( int i = 0; i < nr; i++ ) {
		for( int j = i; j < nr; j++ ) {
			if( i == j ) {
				(*L)[i][j] = 1;
			} else {
				(*L)[i][j] = 0;
			}
		}
	}

	for( int i = 0; i < nr; i++ ) {
		for( int j = 0; j < i; j++ ) {
			(*U)[i][j] = 0;
		}
	}

	for( int i = 0; i < nr; i++ ) {
		pt[i] = i;
	}

	NNMatrix* Ac = A->clone();

	for ( int i = 0; i < (nr -1); i++ ) {

		double max = DBL_MIN;
		int pov = -1;
		//find the largest pivot
		for( int u = i; u < nr; u++ ) {
			if( abs((*Ac)[u][i]) > max ) {
				pov = u;
				max = abs((*Ac)[u][i]);
			}
		}

		//swap two rows 
		if( i != pov ) {
			Ac->swap( i, pov );
			int tv = pt[i];
			pt[i] = pt[pov];
			pt[pov] = tv;
		}

		if ( (*Ac)[i][i] != 0 || abs((*Ac)[i][i]) > 0.000001 ) {
			for( int j = ( i + 1); j < nc; j++ ) {
				(*Ac)[j][i] = (*Ac)[j][i] / (*Ac)[i][i];
				for( int k = ( i + 1 ); k < nr; k++ ) {
					(*Ac)[j][k] = (*Ac)[j][k] - (*Ac)[j][i] * (*Ac)[i][k];
				}
			}
		} else {
			throw runtime_error( "Matrix cannot be LU decomposed" );
		}
	}


	for( int i = 0; i < nr; i++ ) {
		for( int j = 0; j < nr; j++ ) {
			if( j < i ) {
				(*L)[i][j] = (*Ac)[i][j];
			} else {
				(*U)[i][j] = (*Ac)[i][j];
			}
		}
	}

	delete Ac;
}

NNVector* luSolve( NNMatrix * A, NNVector * b ) {
	int nr = A->nrows();
	int nc = A->ncols();

	if( nr != nc ) {
		throw invalid_argument( "The dimension of A is not a square matrix" );
	}

	NNMatrix* L = new NNMatrix( nr, nr );
	NNMatrix* U = new NNMatrix( nr, nr );
	int * pt = new int[nr];

	luDecomp( A, L, U, pt );

	/*//This is from the book, I am not comfortable with it.
	   int t = nr - 1;
	   NNVector * bc = b->clone();
	   for( int i = 0; i < t; i ++ ) {
	   double vt = (*bc)[i];
	   (*bc)[i] = (*bc)[pt[i]];
	   (*bc)[pt[i]] = vt;
	   for( int j = (i + 1); j < nr; j++ ) {
	   (*bc)[j] = (*bc)[j] - (*bc)[i] * (*L)[j][i];
	   }
	   }
	 */

	NNVector* bc = new NNVector( nr );
	for( int i = 0; i < nr; i++ ) {
		(*bc)[i] = (*b)[ pt[i] ];
	}

	NNVector* y = fsub( L, bc );
	NNVector* r = bsub( U, y );


	delete L, delete U, delete pt, delete bc, delete y;

	return r;
}

//******************************************************
//*************** Some other functions *****************
//******************************************************

void mapv( NNVector* v, ftr f ){
	for( int i = 0; i < v->len(); i++ ) {
		(*v)[i] = f( (*v)[i] );
	}
}
