/*
 * Design methodology:
 * 1. All operatons on the vector or matrix will only affect current
 * vector or matrix. 
 * 2. If you need assign current vector or matrix to another one, you 
 * need "clone" operation. What you create, you shall be resiponsible 
 * to destroy.
 * 3. To cooperate with other operations, most of functions will return
 * this pointer.
 * 4. All values in vector and matrix are double type. I have implemented
 * the template version. It's so ugly. I delete it. I have thought it for 
 * a while. Finally, I decided to follow matlab's way. 
 *
 * --nncao 
 */
#ifndef _NNMATRIX_
#define _NNMATRIX_
class NNMatrix;
class NNVector;

#define PI 3.1415926
#define DEBUG  1

/*
 * dot product of two vectors
 */
double dpt( NNVector* nnv1, NNVector* nnv2 );

NNMatrix * vvm( NNVector* nnv1, NNVector* nnv2 );
/*
 * the product of matrix and vector
 */
NNVector* mmv( NNVector* nnv, NNMatrix* nnm );
NNVector* mmv( NNMatrix* nnm, NNVector* nnv );

/*
 * the product of matrix and matrix
 */
NNMatrix* mmx( NNMatrix* nnm1, NNMatrix* nnm2);

/*
 * the product of vector, matrix and vector
 * Since this is very common, I implement it here 
 * just to save space of programming
 */
double vmv ( NNVector * nnv1, NNMatrix * nnm, NNVector* nnv2 );


NNVector * randvc( int len );
NNVector * randvc( int len, double v );
NNMatrix * randmx( int nr );
NNMatrix * randmx( int nr, double v, double d );
NNMatrix * tp( NNMatrix * nnm );
double trace ( NNMatrix * m );
double det ( NNMatrix * m );
double ldet( NNMatrix * m );
double cholDet( NNMatrix * m );
double lcholDet( NNMatrix * m );
double norm2( NNVector* v );

/*
 * These three methods are used to solve the linear systems.
 * One is conjugate gradient method, one is using
 * choleksy decomposition, the last one is using LU decomp
 * I hope they are fast.
 */
NNVector* conjugate( NNMatrix * A, NNVector* b );
NNVector* cholSolve( NNMatrix * A, NNVector* b );
NNMatrix* mCholSolve( NNMatrix * A, NNMatrix * B);
NNVector* luSolve( NNMatrix * A, NNVector* b ); 

//NNMatrix* inverse( NNMatrix * A );
//NNVector* inSolve( NNMatrix* A, NNVector* b );
/* These two functions are very special.
 * They will change the values of input parameter
 * Other functions never do this.
 */
void gaussElim( NNMatrix* A, NNMatrix* b );
void gaussElim( NNMatrix* A );

/* Because this function return two matrices. I can not 
 * return two matrices. Right now, I don't want to use
 * c++ 11. 
 * I have used partial pivoting. It should be enough.
 */
void luDecomp( NNMatrix * A, NNMatrix* L, NNMatrix* U, int* pt );

NNMatrix* cholesky ( NNMatrix * A );
NNVector* fsub( NNMatrix * L, NNVector* b );
NNVector* bsub( NNMatrix * L, NNVector* b );
NNVector* linearSolve( NNMatrix* L, NNVector* b );


typedef double ftr ( double );
void mapv( NNVector* v, ftr f );
class NNVector {
	private:
		int size;
		double * ta;

	public:
		friend class NNMatrix;
		friend double dpt( NNVector* nnv1, NNVector* nnv2 );

		friend NNVector* mmv( NNVector* nnv, NNMatrix* nnm );
		friend NNVector* mmv( NNMatrix* nnm, NNVector* nnv );
	public:
		NNVector( int _size );
		NNVector( int _size, double v );

		int len(){ return size;};


		NNVector* add( NNVector*, int del = 0 );
		NNVector* sub( NNVector*, int del = 0 );

		NNVector* mul( double );
		NNVector* div( double );

		NNVector& operator+ ( NNVector& );
		NNVector& operator- ( NNVector& );
		NNVector& operator* ( double );
		NNVector& operator/ ( double );

		double& operator[] ( int index);

		double dpt( NNVector*, int del = 0 );


		/* It will give a clone of current vector.
		 * Any vectors returned by this have to be deleted
		 */
		NNVector* clone() const;

		/* This function is very special, it didn't change the original one
		 * and it will create a new vector. So, you also need to delete the
		 * vector which is returned by mmx
		 */
		NNVector* mmv ( NNMatrix*, int del = 0 );

		void print( int precision = 4 );

		~NNVector() {delete [] ta;};
};


class NNMatrix {
	private:
		int nr;
		int nc;
		double ** tm;

	public:
		friend class NNVector;
		friend NNVector* mmv( NNVector* nnv, NNMatrix* nnm );
		friend NNVector* mmv( NNMatrix* nnm, NNVector* nnv );
		friend NNMatrix* mmx( NNMatrix* nnm1, NNMatrix* nnm2);
		friend NNMatrix* tp( NNMatrix* nnm );

	public:
		NNMatrix( int _nr, int _nc );
		NNMatrix( int _nr, int _nc, double v );

		static NNMatrix* identy( int _nr );

		int nrows() { return nr; };
		int ncols() { return nc; };

		//swap two rows 
		NNMatrix* swap( int i, int j );

		NNMatrix* add ( NNMatrix *, int del = 0 );
		NNMatrix* sub ( NNMatrix *, int del = 0 );
		NNMatrix* mul ( double );
		NNMatrix* div ( double );

		//operators
		NNMatrix& operator+ ( NNMatrix & );
		NNMatrix& operator- ( NNMatrix & );

		NNMatrix& operator* ( double );
		NNMatrix& operator/ ( double );
		double *  operator[] ( int index) { return tm[index]; };

		NNMatrix* transpose ();
		NNMatrix* tp() ;

		void print( int precision = 4 );

		NNMatrix* mmx( NNMatrix *, int del = 0 );
		NNVector* mmv( NNVector *, int del = 0 );

		NNMatrix* clone() const;

		~NNMatrix() { 
			for( int i = 0; i < nr; i++ ) { 
				delete [] tm[i]; 
			} 
			delete [] tm;
		};
};



#endif
