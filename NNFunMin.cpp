#include <iostream>
#include <stdexcept>
#include <cstdlib>
#include <iomanip>
#include <cmath>
#include "NNFunMin.h"
#include "NNMatrix.h"

using namespace std;

#define ITMAX 7
void NNFunMin::brent( NNVector* xi, NNVector* dir, double a, double b, double c, double tol, double& v, double& fv ) {
	double W = ( 3 - sqrt(5) ) / 2;
	double Z = 1 - 2 * W;

	double x0 = a;
	double x1, x2;
	double x3 = c;
	if ( abs(b-a) > abs(c-b) ) {
		x2 = b;
		x1 = b - ( b - a ) * W;
	} else {
		x1 = b;
		x2 = b + ( c - b ) * W;
	}

	NNVector* vx1 = xi->clone()->sub( dir->clone()->mul( x1 ), 1 );
	double fx1 = func( vx1 );
	delete vx1;

	NNVector* vx2 = xi->clone()->sub( dir->clone()->mul( x2 ), 1 );
	double fx2 = func( vx2 );
	delete vx2;

	int it = 0;
	while (abs(x3-x0) > tol*(abs(x1)+abs(x2)) && it < ITMAX ) {
		if( fx1 < fx2 ) {
			x3 = x2;
			x2 = x1;
			//double x = b + (c-b) * W;
			x1 = x2 - (x2 - x0) * W;

			fx2 = fx1;
			vx1 = xi->clone()->sub( dir->clone()->mul( x1 ), 1 );
			fx1 = func( vx1 );
			cout << "x1 : " << x1 << " fx1: " << fx1 << endl;
			delete vx1;
		} else {
			x0 = x1;
			x1 = x2;
			x2 = x1 + (x3-x1) * W;

			fx1 = fx2;
			vx2 = xi->clone()->sub( dir->clone()->mul( x2 ), 1 );
			fx2 = func( vx2 );
			cout << "x2 : " << x2 << " fx2: " << fx2 << endl;
			delete vx2;
		}
		it++ ;
	}
	if( fx1 > fx2 ) {
		v = x2;
		fv = fx2;
	} else {
		v = x1;
		fv = fx1;
	}
}

void NNFunMin::expSearch( NNVector* xi, NNVector* dir, double& a, double& b, double& c, int size ) {
	NNVector* va = xi->clone()->add( dir->clone()->mul( a ), 1 );
	double fa = func( va );
	delete va;

	NNVector* vb = xi->clone()->add( dir->clone()->mul( b ), 1 );
	double fb = func( vb );
	delete vb;

	NNVector* vc = xi->clone()->add( dir->clone()->mul( c ), 1 );
	double fc = func( vc );
	delete vc;

	int K = 1;
	double W = (double) 2 / (sqrt(5) - 1);
	double b2 = b;
	cout << setprecision( 8 ) << endl;
	cout << "fa: " << fa << "fb: " << fb << "fc : " << fc << endl;
	while( fa < fb || fb > fc ) {
		a = b;
		fa = fb;

		b2 = b;
		b = c;
		fb = fc;

		c = ( c - b2 ) * W + c;
		vc = xi->clone()->add( dir->clone()->mul( c ), 1 );
		fc = func( vc );
		delete vc;

		cout << "a : " << a << " b : " << b << " c : " << c << endl;
		cout << "fa: " << fa << "fb: " << fb << "fc : " << fc << endl;
		K++;
		if( K > size ) {
			throw runtime_error( "Can not find good points for search!" );
		}
	}
}

//int NNFunMin::steepest( NNVector* xi, NNVector* dir, double& a, double& b, double& c, int size ) {
//}

int NNFunMin::expSearch2( NNVector* xi, NNVector* dir, double& a, double& b, double& c, int size ) {
	double step = (b-a);
	double norm = sqrt( dpt(dir,dir) );

	NNVector* vb = xi->clone()->sub( dir->clone()->mul( b ), 1 );
	double fb = func( vb );
	delete vb;

	double W = (double) 2 / (sqrt(5) - 1);
	cout << setprecision( 16 ) << endl;
	//int smax = 10;
	int i = 0;

	bool bk = false;
	//determine which direction to search + or -
	c = 0.1;
	NNVector* vc = xi->clone()->sub( dir->clone()->mul( c ), 1 );
	double fc = func( vc );
	delete vc;
	if( !isnan(fc) && fc < fb ) {
		cout << "Seach on + direction." << endl;
		for( i = 1; i < norm; i++ ) {
			double bc = i/ double(norm);
			NNVector* vbc = xi->clone()->sub( dir->clone()->mul( bc ), 1 );
			double fbc = func( vbc );
			cout << "fbc " << fbc << endl;
			cout << "bc :" << bc << endl;
			delete vbc;
			if( isnan( fbc ) ) {
				bk = true;
				break;
			}
			if( fbc < fb ) {
				fb = fbc;
				b = bc;
				a = b - step;
				c = b + step;
			}
		}
	}

	a = -0.1;
	NNVector* va = xi->clone()->sub( dir->clone()->mul( a ), 1 );

	double fa = func( va );
	delete va;
	if( !isnan(fa) && fa < fb ) {
		cout << "Seach on - direction." << endl;
		for( i = 1; i < norm; i++ ) {
			double bc = -i/ double(norm);
			NNVector* vbc =  xi->clone()->sub( dir->clone()->mul( bc ), 1 );
			double fbc = func( vbc );
			cout << "fbc " << fbc << endl;
			cout << "bc :" << bc << endl;
			delete vbc;
			if( isnan( fbc ) ) {
				bk = true;
				break;
			}
			if( fbc < fb ) {
				fb = fbc;
				b = bc;
				a = b - step;
				c = b + step;
			}
		}
	}
	//b may be the smallest one on the other direction here.

	//from this direction, find the smallest point to search

	if( isnan(fa) && isnan(fc) ) {
		cout << "This should be fine. We set b = 0" << endl;
		a = b - step;
		c = b + step;
	}
	if( bk ) {
		cout << "Even it is in the middle, we need to search!" << endl;
		a = b - step;
		c = b + step;
	}
	//we have some situations here:
	//1. b didn't move at all
	//2. b is in the middle of + or - direction
	//3. b is in the end of + or - direction

	if( abs(b) < 1e-3 || abs(abs(b)-0.9) < 1e-3 ) {
		cout<< "b didn't move at all or b is at the end, set values of a and c and search" << endl;
		a = b - step;
		c = b + step;
	}

	va = xi->clone()->sub( dir->clone()->mul( a ), 1 );
	fa = func( va );
	delete va;

	vc = xi->clone()->sub( dir->clone()->mul( c ), 1 );
	fc = func( vc );
	delete vc;
	//Use exponential search to find the minimal value
	int K = 1;

	cout << "a: " << a << "b: " << b << "c: " << c << endl;
	cout << "fa: " << fa << "fb: " << fb << "fc: " << fc << endl;

	if( abs( b ) < 1e-3 || abs(abs(b)-0.9) < 1e-3 || bk ) { // we need exponential search
		while( fb >= fa ) { // && abs(a) < 1 
			c = b;
			fc = fb;

			b = a;
			fb = fa;

			a = ( b - c ) * W + b;
			va = xi->clone()->sub( dir->clone()->mul( a ), 1 );
			fa = func( va );
			delete va;

			cout<< "a: " << a << " fa: " << fa << endl;
			if( isnan(fa) ){ //  || fa < 0
				c = fb;
				return 1;
			}
		}
		if( fa > fb  ) { // && fc > fb
			return 2;
		}
		double ofc = fc;
		while( fb >= fc ) { // && abs(c) < 1 
			a = b;
			fa = fb;

			b = c;
			fb = fc;

			c = ( b - a ) * W + b;
			vc = xi->clone()->sub( dir->clone()->mul( c ), 1 );
			fc = func( vc );

			cout << "c: " << c << " fc: " << fc << endl;
			if( isnan(fc) ){ // || fc < 0
				c = fb;
				return 1;
			}
			delete vc;
		}
		return 2;
	}
	cout<< "b is in the middle of some values. so, use it" << endl;
	a = b - 0.1;
	c = b + 0.1;
	cout << "a: " << a << "b: " << b << "c: " << c << endl;
	return 2;
}

int NNFunMin::expSearch3( NNVector* xi, NNVector* dir, double& a, double& b, double& c, int size ) {
	double step = (b-a);
	double norm = sqrt( dpt(dir,dir) );

	NNVector* vb = xi->clone()->sub( dir->clone()->mul( b ), 1 );
	double fb = func( vb );
	delete vb;

	double W = (double) 2 / (sqrt(5) - 1);
	cout << setprecision( 16 ) << endl;
	//int smax = 10;
	int i = 0;

	bool bk = false;
	//determine which direction to search + or -
	int inr = (int) norm / 10;
	c = 1/norm * inr;
	NNVector* vc = xi->clone()->sub( dir->clone()->mul( c ), 1 );
	double fc = func( vc );
	delete vc;
	if( !isnan(fc) && fc < fb ) {
		cout << "Seach on + direction." << endl;
		for( i = 2; i < 10; i++ ) {
			double bc = i/ double(norm) * inr;
			NNVector* vbc = xi->clone()->sub( dir->clone()->mul( bc ), 1 );
			double fbc = func( vbc );
			cout << "fbc " << fbc << endl;
			cout << "bc :" << bc << endl;
			delete vbc;
			if( isnan( fbc ) ) {
				bk = true;
				break;
			}
			if( fbc < fb ) {
				fb = fbc;
				b = bc;
				a = b - step;
				c = b + step;
			}
		}
	}
	//b may be the smallest one on the other direction here.
	//Here, I want to decide the step size, from 0.003, each time
	//decrease 10 times, at most four times is fine.
	step = step * 10;
	do {
		step  = step * 0.1;
		vc = xi->clone()->sub( dir->clone()->mul( c ), 1 );
		fc = func( vc );
		delete vc;
	} while( isnan( fc ) );
	//from this direction, find the smallest point to search
	if( isnan(fc) ) {
		cout << "This should be fine. We set b = 0" << endl;
		a = b - step;
		c = b + step;
	}
	if( bk ) {
		cout << "Even it is in the middle, we need to search!" << endl;
		a = b - step;
		c = b + step;
	}
	//we have some situations here:
	//1. b didn't move at all
	//2. b is in the middle of + or - direction
	//3. b is in the end of + or - direction

	if( abs(b) < 1e-3 || abs(abs(b)-0.9) < 1e-3 ) {
		cout<< "b didn't move at all or b is at the end, set values of a and c and search" << endl;
		a = b - step;
		c = b + step;
	}


	//Use exponential search to find the minimal value
	int K = 1;

	cout << "b: " << b << "c: " << c << endl;
	cout << "fb: " << fb << "fc: " << fc << endl;

	if( abs( b ) < 1e-3 || abs(abs(b)-0.9) < 1e-3 || bk ) { // we need exponential search
		double ofc = fc;
		int it = 0;
		while( fb >= fc ) { // && abs(c) < 1 
			a = b;
			//fa = fb;

			b = c;
			fb = fc;

			c = ( b - a ) * W + b;
			vc = xi->clone()->sub( dir->clone()->mul( c ), 1 );
			fc = func( vc );

			cout << "c: " << c << " fc: " << fc << endl;
			if( isnan(fc) || it == ITMAX ){ // || fc < 0
				c = fb;
				delete vc;
				return 1;
			}
			delete vc;
			it++;
		}
		return 2;
	}
	cout<< "b is in the middle of some values. so, use it" << endl;
	a = b - 1/norm;
	c = b + 1/norm;
	cout << "a: " << a << "b: " << b << "c: " << c << endl;
	return 2;

}
void NNFunMin::pabInp( NNVector* xi, NNVector* dir, double a, double b, double c, double eps, double v, double fv ) {
}

void NNFunMin::min ( NNVector * xi, int itmax, double step ) {
	double eps = 1.0e-18;
	double tol = 1.0e-8;

	NNVector* x = xi->clone();
	cout << "x starts at : " << endl;
	x->print();
	double fx = func( x );
	cout << "fx :" << fx << endl;
	NNVector* h = fund( x );
	cout << "first fund " << endl;
	h->print();
	
	NNVector* lgx = h->clone();


	double glim = 10;
	for( int i = 0; i < itmax; i++ ) {
		double v = 0.0;
		double fv = 0.0;
		double b = 0;
		double a = b - 3 * step;
		double c = b + 3 * step;
		int sear = expSearch3( x, h, a, b, c, 90 );
		if( sear == 2 ) {
			brent( x, h, a, b, c, tol, v, fv );
		} else {
			v = b;
			fv = c;
		}
		cout<< "V is : " << v << endl;

		//Use common method to update 
		x->sub( h->clone()->mul( v ), 1 );


		cout<< "X is : " << endl;
		x->print();
		cout<< "D1 is : " << endl;
		h->print();
		cout<< "fv :" << fv << endl;
		cout<< "fx :" << fx << endl;
		if( 2.0 * abs(fx-fv) < tol*( abs(fx)+abs(fv)+eps ) )
			break;
		fx = fv;
		NNVector* gxn = fund( x );

		double lgt = dpt( lgx, lgx );

		if( lgt < 1 ){
		   break;
		}
		double gama = gxn->dpt( gxn->clone()->sub(lgx), 1 ) / lgt;
		NNVector* ht = gxn->clone()->sub( h->clone()->mul( gama ), 1 );

		delete h, delete lgx;
		lgx = gxn;
		h = ht;
	}
	int len = xi->len();
	for( int i = 0; i < len; i++ ) {
		(*xi)[i] = (*x)[i];
	}
	
	delete x, delete lgx, delete h;
}

