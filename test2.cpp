#include <fstream>
#include <iostream>
#include <cstdlib>
#include <cmath>

#include "NNMatrix.h"
#include "NNGp.h"
#include "NNCov.h"

using namespace std;

NNMatrix* getData( const char* fname ) {
	NNMatrix* data = new NNMatrix( 5, 30 );
	fstream ifs ( fname );
	for( int i = 0; i < 5; i++ ) {
		for( int j = 0; j < 30; j++ ) {
			ifs >> (*data)[i][j];
		}
	}
	ifs.close();
	return data;
}
pair<NNMatrix*, NNVector*> getInData( NNMatrix* data ) {
	NNMatrix* ix = new NNMatrix( 30, 2);
	NNVector* iy = new NNVector( 30 );
	for( int i = 0; i < 5; i++ ) {
		for( int j = 0; j < 30; j++ ) {
			if( (j % 5) == i ) {
				(*ix)[j][0] = i;
				(*ix)[j][1] = j;
				(*iy)[j] = (*data)[i][j];
			}
		}
	}
	return make_pair( ix, iy );
}

pair<NNMatrix*, NNVector*> getTestData( NNMatrix* data ) {
	NNMatrix* tx = new NNMatrix( 30, 2 );
	NNVector* ty = new NNVector( 30 );
	for( int i = 0; i < 5; i++ ) {
		for( int j = 0; j < 30; j++ ) {
			if( ((j+2) % 5) == i ) {
				(*tx)[j][0] = i;
				(*tx)[j][1] = j;
				(*ty)[j] = (*data)[i][j];
			}
		}
	}
	return make_pair( tx, ty );
}
void getHyp( const char* hname, NNVector* loghyp, double& lnv, double& prior ) {
	fstream ifs( hname );
	ifs >> prior;
	ifs >> (*loghyp)[1];
	ifs >> (*loghyp)[2];
	ifs >> (*loghyp)[0];
	ifs >> lnv;
	(*loghyp)[1] /= 5;
	(*loghyp)[2] /= 5;
	mapv( loghyp, log );
	lnv = log( lnv );
	ifs.close();
}
void printhnp( NNVector* loghyp, double lnv, double prior );
void test1() {
	const char* fname = "map4";
	const char* hname = "hyper4";
	NNMatrix* data = getData( fname );
	pair<NNMatrix*, NNVector*> ixy= getInData( data );
	NNMatrix* ix = ixy.first;
	NNVector* iy = ixy.second;
	pair<NNMatrix*, NNVector*> txy = getTestData( data );
	NNMatrix* tx = txy.first;
	NNVector* ty = txy.second;
	NNVector* loghyp = new NNVector( 3 );
	double lnv = 0; 
	double prior = 23;
	NNCov* cf = new NNCovSe();

	srand( time( NULL ) );
	(*loghyp)[0] = 15;
	(*loghyp)[1] = 15;
	(*loghyp)[2] = 15;
	mapv( loghyp, log );
	lnv = log(1);

	cout<< "Prediction data results before training: " << endl;
	NNVector* tyr1 = NNGp::regm( loghyp, lnv, cf, ix, iy, tx, prior );
	tyr1->print();


	cout<< "train starts: " << endl;
	NNGp::train( loghyp, lnv, prior, cf, ix, iy );
	printhnp( loghyp, lnv, prior );
	cout<< "Train data results: " << endl;
	NNVector* tyr2 = NNGp::regm( loghyp, lnv, cf, ix, iy, tx, prior );
	tyr2->print();
	//delete tyr2;
	

	getHyp( hname, loghyp, lnv, prior );
	NNVector* tyr = NNGp::regm( loghyp, lnv, cf, ix, iy, tx, prior );
	cout<< "Other train data results:" << endl;
	tyr->print();

	cout<< "Test data original values: " << endl;
	ty->print();

	tyr2->sub( ty );
	tyr->sub( ty );
	double err = dpt( tyr2, tyr2 );
	cout<< "Train Error : " << err << endl;
	err = dpt( tyr, tyr );
	cout<< "Train Error : " << err << endl;
	delete ix, delete iy, delete tx, delete ty;
	delete cf, delete data, delete tyr1, delete tyr; //tyr
	delete tyr2, delete loghyp;
}
void loadData( NNMatrix* x, NNVector* y ) {
	fstream fi ( "map" );
	int len = y->len();
	for( int i = 0; i < len; i++ ) {
		fi >> (*x)[i][0];
	}
	for( int i = 0; i < len; i++ ) {
		fi >> (*y)[i];
	}
}
void test2() {
	NNMatrix * ix = new NNMatrix( 200, 1 );
	NNVector* iy = new NNVector( 200 ); 
	loadData( ix, iy );
	NNMatrix* tx = tp( ix );
	tx->print();
	iy->print();
	NNCov* cf = new NNCovSe();
	NNVector* loghyp = new NNVector( 2 );
	(*loghyp)[0] = 2;
	(*loghyp)[1] = 2;
	double lnv = log(0.2);
	mapv( loghyp, log );
	double prior = 15;

	cout<< "train starts: " << endl;
	NNGp::train( loghyp, lnv, prior, cf, ix, iy );
	printhnp( loghyp, lnv, prior );
	delete ix, delete tx, delete iy, delete loghyp;
}
void printhnp( NNVector* loghyp, double lnv, double prior ){
	NNVector* hyp = loghyp->clone();
	mapv( hyp, exp );
	double nv = exp( lnv );
	cout << "Hyps are : " << endl;
	hyp->print();
	cout << "nv : " << nv << " prior : " << prior << endl;
}
int main() {
	setbuf( stdout, NULL );
	test1();
	test2();
	return 0;
}

