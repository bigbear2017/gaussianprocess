#include <cmath>

#include "NNCov.h"
#include "NNMatrix.h"
#include "NNException.h"

NNMatrix* NNCovSe::cf( NNVector* loghyp, NNMatrix* d1, NNMatrix* d2) {
	NNVector* hyp = loghyp->clone();
	mapv( hyp, exp );
	int nr1 = d1->nrows();
	int nc1 = d1->ncols();

	NNMatrix* dc = NULL;
	if( d2 == NULL ) {
		 dc = d1->clone();
	} else {
		dc = d2;
	}
	int nr2 = dc->nrows();
	int nc2 = dc->ncols();
	if( nc1 != nc2 ) {
		throw DimException( d1, dc );
	}
	if( hyp->len() != ( nc1 + 1 )){
		throw DimException( hyp, d1 );
	}
	NNMatrix * r = new NNMatrix( nr1, nr2 );
	for( int i = 0; i < nr1; i++ ) {
		for( int j = 0; j < nr2; j++ ) {
			double sum = 0;
			for( int k = 0; k < nc1; k++ ) {
				double t = ((*d1)[i][k] - (*dc)[j][k]);
				t = t / (*hyp)[ k + 1 ];
				t = t * t;
				sum += t;
			}
			sum = - sum / 2 ;
			double sv2 = (*hyp)[0] * (*hyp)[0];
			(*r)[i][j] = sv2 * exp(sum);
		}
	}
	if( d2 == NULL ) {
		delete dc;
	}
	delete hyp;
	return r;
}

NNMatrix* NNCovSe::dvt( NNVector* loghyp, NNMatrix* d, int index ) {
	NNVector* hyp = loghyp->clone();
	mapv( hyp, exp );
	int nr = d->nrows();
	int nc = d->ncols();
	int len = hyp->len();
	if( len != ( nc + 1 ) ){
		throw DimException( hyp, d );
	}

	NNMatrix* dc = d->clone();
	NNMatrix* r = new NNMatrix( nr, nr );
	NNMatrix* K = new NNMatrix( nr, nr );
	for( int i = 0; i < nr; i++ ) {
		for( int j = 0; j < nr; j++ ) {
			double sum = 0;
			for( int k = 0; k < nc; k++ ) {
				double t = ((*d)[i][k] - (*dc)[j][k]);
				t = t / (*hyp)[ k + 1 ];
				t = t * t;
				if ( index == (k + 1) ) {
					(*K)[i][j] = t;
				}
				sum += t;
			}
			sum = - sum / 2 ;
			(*r)[i][j] = exp(sum);
			//(*k)[i][j] = sum;
		}
	}
	delete dc;
	double sv2 = (*hyp)[0] * (*hyp)[0];
	if( index == 0 ) {
		//return the deravitive of signal variance 
		r->mul( 2 )->mul( sv2 );
		delete K, delete hyp;
		return r;
	} else {
		//return the deravitive of each length scale
		r->mul( sv2 );
		//double ih = 1 / (*hyp)[index];
		for( int i = 0; i < nr; i++ ) {
			for( int j = 0; j < nr; j++ ) {
				(*r)[i][j] = (*r)[i][j] * (*K)[i][j];
			}
		}
		delete K, delete hyp;
		return r;
	}
}
