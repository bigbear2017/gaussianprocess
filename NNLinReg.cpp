#include "NNLinReg.h"
#include "NNMatrix.h"

NNVector* NNLinReg::train( NNMatrix* x, NNVector* y ) {
	//beta = (x^T x )-1x^t y
	NNMatrix * xt = tp( x );
	NNMatrix * A = mmx( xt, x );
	NNVector * b = mmv( xt, y );
	NNVector * r = conjugate( A , b );
	delete xt, delete A, delete b;
	return r;
}

NNVector* NNLinReg::predict( NNMatrix* x, NNVector* beta ) {
	NNVector* y = mmv( beta, x );
	return y;
}
