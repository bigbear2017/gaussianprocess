/*
 * This class will provide me some useful functions about array. 
 * Hopefully, I will implement all the functions in the arrays in Java.
 */
#ifndef _NNARRAY_
#define _NNARRAY_

class NNArray { 
	public:
		static int mindex ( double * arr, int size );
		//static int mindex ( int * arr, int size );
		//static int mindex ( char * arr, int size );

		static int maxdex ( double * arr, int size);
		//static int maxdax ( int * arr, int size );

		//static void fill ( int * arr, int size, int v = 0 );
		//static void fill ( int * arr, int size, int v = 0 );
};

int NNArray::mindex( double * arr, int size ) {
	double min = DBL_MAX;
	int index = -1;
	for( int i = 0; i < size; i++ ) {
		if( arr[i] < min ) {
			min = arr[i];
			index = i;
		}
	}
	return index;
}

int NNArray::maxdex( double * arr, int size ) {
	double max = DBL_MIN;
	int index = -1;
	for( int i = 0; i < size; i++ ) {
		if( arr[i] > max ) {
			max = arr[i];
			index = i;
		}
	}
	return index;
}

#endif
