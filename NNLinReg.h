#ifndef _LINREG_
#define _LINREG_
#include "NNMatrix.h"
class NNLinReg {
	public:
		static NNVector* train ( NNMatrix *, NNVector* );
		static NNVector* predict( NNMatrix*, NNVector* );
};

#endif

