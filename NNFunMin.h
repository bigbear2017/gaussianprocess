#ifndef _NNFUNMIN_
#define _NNFUNMIN_
#include "NNMatrix.h"
#include "NNException.h"

class NNFunMin {
	public:
	virtual double func ( NNVector * ) = 0; //
	virtual NNVector* fund ( NNVector * ) = 0;
	void brent ( NNVector* xi, NNVector* dir, double a, double b, double c, double tol, double& v, double& fv );
	void expSearch( NNVector* xi, NNVector* dir,double& a, double& b, double& c, int size = 20 );
	int expSearch2( NNVector* xi, NNVector* dir,double& a, double& b, double& c, int size = 20 );
	int expSearch3( NNVector* xi, NNVector* dir,double& a, double& b, double& c, int size = 20 );
	void pabInp( NNVector* xi, NNVector* dir, double a, double b, double c, double eps, double v, double fv );
	void min ( NNVector * xi, int itmax = 100, double step = 1e-3 );
};
#endif

