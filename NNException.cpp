#include <string>
#include <sstream>

#include "NNException.h"
#include "NNMatrix.h"

using namespace std;
DimException::DimException( NNVector * nnv1, NNVector * nnv2 ) {
	stringstream ss;
	ss << "Dimension Exception: \n" << "Vector1 length: " << nnv1->len() <<"   " \
		<< "Vector2 length: " << nnv2->len() <<endl;
	str = ss.str();
}

DimException::DimException( NNMatrix * nnm1, NNMatrix * nnm2 ) {
	stringstream ss;
	ss << "Dimension Exception: \n" << "Matrix1 size: " << nnm1->nrows() << "x" \
		<< nnm1->ncols() << "   " <<  "Matrix2 size: " << nnm2->nrows() << "x" \
		<< nnm2->ncols() <<endl;
	str = ss.str();
}
DimException::DimException( NNVector * nnv,  NNMatrix * nnm ) {
	stringstream ss;
	ss << "Dimension Exception: \n" << "Vector length: " << nnv->len() \
		<< "   " << "Matrix size: " << nnm->nrows() << "x" << nnm->ncols() <<endl;
	str = ss.str();
}
DimException::DimException( NNMatrix * nnm,  NNVector * nnv ) {
	stringstream ss;
	ss << "Dimension Exception: \n" << "Matrix size: " << nnm->nrows() << "x" \
		<< nnm->ncols() << "   " << "Vector length: " << nnv->len() <<endl;
	str = ss.str();
}

