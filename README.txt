This project is aim to implement GP training and regression.
To do this, I have to implement some classes to support the 
following things:
1. matrix and vector modeling and some common operations
2. matrix inverse using Cholesky Decomposition, Gaussian elimination and conjugate gradient method
3. conjugate gradient optimization and brent method to minimize functions

Q1. How to compile?
Use make

Q2. How to train a GP hyperparameters?
load the data and use NNGp.train 

Q3. How to do regression?
load the data and trained hyperparameters, use NNGp.regm

Q4. What license?
I always use Linux OS. So, it is under GPL. It's just for fun.
